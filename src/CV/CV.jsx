export const CV = {
    hero: {
        name: "Jose",
        surname: "Almendros Corpas",
        city: "Málaga",
        email: "joseralmendros@gmail.com",
        birthDate: "03/02/1990",
        phone: "6752507391",
        image: "https://cdn.discordapp.com/attachments/846417268506099735/929032210349170709/WhatsApp_Image_2022-01-07_at_16.21.28.jpeg",
        gitLab: "https://gitlab.com/Deproweb",
        aboutMe: [
        {
            info: " 💻 Full Stack Developer",
        },
        {
            info: " 📱 Amante de las tecnologías, el desarrollo web, los videojuegos y la lectura",
        },
        {
            info: " 🦾  Motivación, dedicación y ganas de seguir aprendiendo como lema",
        },
        {
            info: "👨🏻‍💻 REACT, NODE, JS, ANGULAR, JS, HTML, CSS, AGILE, PHP",
        },
        ],
    },
    education: [
        {
        name: "Bootcamp Full Stack Developer",
        date: "2022",
        where: "Upgrade Hub",
        },
        {
        name: "Master en Enseñanza del español como lengua extranjera",
        date: "2020",
        where: "UNIR",
        },
        {
        name: "Grado en Educación Primaria",
        date: "2016",
        where: "Universidad de Málaga",
        },
    ],
    certificates: [
        {
        name: "Curso de React",
        date: "2021",
        where: "Udemy",
        },
        {
        name: "Curso de GIT",
        date: "2021",
        where: "Platzi",
        },
        {
        name: "Curso de HTML, CSS y JS",
        date: "2021",
        where: "Udemy",
        },
    ],
    experience: [
        {
            name: "Profesor de español",
            date: "Sept-19 - Nowadays",
            where: "ACCEM",
        },
        {
            name: "Educador",
            date: "Oct 2017 - Sept 2019",
            where: "ACCEM",
        },
        {
            name: "Profesor de primaria",
            date: "Dic 2016 - Mayo 2017",
            where: "Asociación Trans",
        },
    ],
    languages: [{
        language: "Español",
        level: "Native",
    },
    {
        language: "Inglés",
        level: "Medio, título de B1",
    }],
    habilities: [
        "HTML y CSS",
        "ANGULAR",
        "REACT",
        "NODE",
        "GIT",
        "AGILE",
    ],
    volunteer: [
        {
        name: "Profesor de ESA",
        where: "Cáritas",
        },
        
    ],
    };