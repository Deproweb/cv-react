//Styles
import './App.scss';

//Hooks
import { useState } from "react";

//Cv
import { CV } from "./CV/CV";

//Components
import Experience from './components/Experience/Experience';
import Info from './components/Info/Info';
import More from './components/More/More';
import Education from './components/Education/Education';
import About from './components/About/About';

//Boostrap

import Button from 'react-bootstrap/Button';

const { hero, education, certificates, experience, languages, habilities, volunteer } = CV;

function App() {

  const [showEducation, setShowEducation] = useState (false);
  const [showExperience, setShowExperience] = useState (false);

  const educationFn = () => {
    setShowEducation(!showEducation)
    setShowExperience(false)
  }

  const experienceFn = () => {
    setShowExperience(!showExperience)
    setShowEducation(false)
  }

  return (
    <div className="App">
      <Info hero={hero}/>
      <About hero={hero}/>
      <div className="buttons">
      <Button variant="success" className="custom-btn btn-4" onClick={educationFn} > Education</Button>
      <Button variant="success" className="custom-btn btn-4" onClick={experienceFn} > Experience </Button>
      </div>
    <div>
    {showEducation && (<Education certificates = {certificates} education={education} /> )}
    {showExperience && <Experience experience={experience}></Experience>}
    </div>
      <More languages={languages} habilities={habilities} volunteer={volunteer}/>
    </div>
  );
}

export default App;
