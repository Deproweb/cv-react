    import './Experience.scss'

    export default function Experience ({experience}) {

        return (<div className="cv__part">
            {experience.map(element => 
                <div className="cv__map" key={element.name}>
                    <p className="cv__map__title">🧑🏻‍🏫{element.name}</p>
                    <p>{element.where}</p>
                    <p>{element.date}</p>
                </div>
            )}
        </div>)

    }