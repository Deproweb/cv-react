import "./Info.scss"

export default function Info ({hero}) {

    return (
    
    <div className="hero">
        <img src={hero.image} alt="" />
        <div className="cv__part">
            <h2>{hero.name} {hero.surname}</h2>
            <p>🗺️ {hero.city} </p>
            <p>🗓️ {hero.birthDate}</p>
            <p>📧 <a className="ancle" href={"mailto:" + hero.email}>joseralmendros@gmail.com</a></p>
            <p>📱 {hero.phone}</p>
            <p>💾 <a href="https://gitlab.com/Deproweb" rel="noreferrer" target="_blank" className="ancle">GitLab</a></p>
        </div>
    </div>
    )

}