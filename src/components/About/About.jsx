
export default function About ({hero}) {

    return (<div className="cv__part">
        {hero.aboutMe.map(element => 
            <div key={element.info}>
                <p>{element.info}</p>
            </div>
        )}
    </div>);
};