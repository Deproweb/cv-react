import './Education.scss'


export default function Education ({education, certificates}) {

    return (<>
            <div className="cv__part">
            { education.map(element => 
                <div className="cv__map" key={JSON.stringify(element)}>
                <p className="name">📕 {element.name}</p>
                <p>{element.where}</p>
                <p>{element.date}</p>
                </div>
            )}
        </div>
        <div className="cv__part">
            {certificates.map(element => 
                <div className="cv__map" key={JSON.stringify(element)}>
                <p className="name">🖥 {element.name}</p>
                <p>{element.where}</p>
                <p>{element.date}</p>
                </div>    
            )}
        </div>

        </>)
}

