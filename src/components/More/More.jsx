

const More = ({languages, habilities}) => {

    return (
        <>
            <div className="cv__part">
                <h4>Idiomas</h4>
                {languages.map((element => 
                    <div key={element.language}>
                        <p>💬 {element.language} - {element.level}</p>
                    </div>
                    ))}
            </div>
        </>    
        )
};

export default More